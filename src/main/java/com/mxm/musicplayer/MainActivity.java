package com.mxm.musicplayer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements OnClickListener{
   private MediaPlayer mediaPlayer = new MediaPlayer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button play = (Button) findViewById(R.id.play);
        Button pause = (Button) findViewById(R.id.pause);
        Button stop = (Button) findViewById(R.id.stop);
        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        stop.setOnClickListener(this);
       /*mediaPlayer.setDataSource(path);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.prepareAsync();
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayer.start();
            }
        });*/
         mediaPlayer=MediaPlayer.create(this, R.raw.a);
       /* if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.
                WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            initMediaPlayer();
        }*/
        }
        private void initMediaPlayer () {
            try {
                File file = new File(Environment.getExternalStorageDirectory(), "a.mp3");
                mediaPlayer.setDataSource(file.getPath());
                mediaPlayer.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onRequestPermissionResult(int requestCode,String[] permissions,int[] grantResult){
        switch (requestCode){
            case 1:
                if (grantResult.length>0 && grantResult[0]==PackageManager.PERMISSION_GRANTED){
                    initMediaPlayer();
                }else {
                    Toast.makeText(this,"拒绝权限将无法使用程序",Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
                default:
        }
        }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.play:
            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
            }
            break;
            case  R.id.pause:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }
                break;
            case R.id.stop:
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.reset();
                    initMediaPlayer();
                }
                break;
                default:
                    break;
        }
    }
    public void  onDestroy() {

        super.onDestroy();
        if (mediaPlayer !=null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }
}